# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  alphabet = ("a".."z").to_a
  str.chars.reject { |ch| alphabet.include?(ch) }.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  num = str.length
  if num.odd?
    return str[num / 2]
  else
    return str[num / 2 - 1..num / 2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowels = "aeiou"
  counter = 0
  str.each_char do |ch|
    if vowels.include?(ch)
      counter += 1
    end
  end
  counter
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result = ""
  return result if arr.length == 0
  arr.each_index do |idx|
    result << arr[idx] + separator unless idx == arr.length - 1
  end
  result + arr.last
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  result = ""
  str.chars.each_index do |idx|
    if (idx+1).odd?
      result << str[idx].downcase
    else
      result << str[idx].upcase
    end
  end
  result
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  result = []
  str.split.each do |word|
    if word.length > 4
      result << word.reverse
    else
      result << word
    end
  end
  result.join " "
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  integers = (1..n).to_a
  result = []
  integers.each do |int|
    if int % 5 == 0 && int % 3 == 0
      result << "fizzbuzz"
    elsif int % 5 == 0
      result << "buzz"
    elsif int % 3 == 0
      result << "fizz"
    else
      result << int
    end
  end
  result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  result = []
  (1..num).each do |i|
    if num % i == 0
      result << i
    end
  end
  return true if result.length == 2
  false
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result = []
  (1..num).each do |i|
    if num % i == 0
      result << i
    end
  end
  result
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |i| prime?(i)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  num_even = arr.select(&:even?)
  num_odd = arr.select(&:odd?)
  return num_even[0] if num_even.length == 1
  num_odd[0]
end
